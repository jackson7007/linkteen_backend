Backend to linkteen, with login and password already built with jwt

# Technologies

- Backend in nodejs with Expressjs and mongoose with MongoDB

# Support

- Uploads with authenticated routes
- Https

# SETUP

- Install MongoDB

```
https://docs.mongodb.com/manual/administration/install-community/

```

- Install Nodejs

```
https://nodejs.org/en/download/package-manager/
```

```
sudo npm install -g nodemon
```

```
cp config.js.dist config.js
npm install
nodemon index.js
```

## Go to:

http://localhost:9090/api

### this is just to see word online

Any help or pull request is welcome!

:airplane:
