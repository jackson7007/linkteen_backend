const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt-nodejs");
const config = require("../../../config");
const Usuario = require("../model");

/*Usuario Routes*/
exports.login = async (req, res) => {
  const user = await Usuario.findOne({ login: req.body.login, ativo: true });

  if (!user) {
    res.json({
      success: false,
      message: "Usuário não encontrado",
      login: req.body.login,
    });
  } else {
    bcrypt.compare(req.body.password, user.password, function (err, ok) {
      if (ok) {
        console.log("Login de usuário: " + user.nome + " : " + user.login);

        var beAToken = {};
        beAToken.login = user.login;
        beAToken._id = user._id;
        beAToken.nome = user.nome;
        beAToken.role = user.role;
        beAToken.type = user.type;

        var token = jwt.sign(beAToken, config.secret, {
          expiresIn: "1d", // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: "Login efetuado com sucesso!",
          token: token,
          id: user._id,
          type: user.type,
        });
      } else {
        res.json({ success: false, message: "Usuário não encontrado" });
      }
    });
  }
};

exports.new = async (req, res) => {
  const user = await Usuario.findOne({ login: req.body.login });
  if (!user) {
    var model = new Usuario(req.body);
    const data = await model.save();

    if (data) {
      const ret = {};
      ret._id = data._id;
      ret.login = data.login;
      ret.type = data.type;
      ret.ativo = data.ativo;
      res.json({ success: true, data: ret, form: req.body });
    } else {
      res.json({ succsess: false, data: ret, form: req.body });
    }
  } else {
    res.json({
      succsess: false,
      data: { text: "Usuario já existe ou está inativo" },
      form: req.body,
    });
  }
};
