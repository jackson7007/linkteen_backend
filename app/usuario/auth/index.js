const express = require("express");
const rotas = express.Router();
const controller = require("./controller");
rotas.post("/login", controller.login);
rotas.post("/new-user", controller.new);
module.exports = rotas;
