const express = require("express");
const rotas = express.Router();

const controller = require("./controller");

const multer = require("multer");
const config = require("../../config");
const upload = multer({ dest: config.uploadPath });
rotas.get("/image/:hash", controller.getImage);
rotas.post("/upload", upload.single("file"), controller.upload);

rotas.post("/", controller.new);
rotas.put("/:id", controller.edit);
rotas.get("/", controller.index);
rotas.get("/:id", controller.show);
rotas.delete("/:id", controller.delete);

module.exports = rotas;
