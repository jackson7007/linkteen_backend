const Model = require("./model");

exports.upload = (req, res) => {
  res.json(req.file);
};

exports.getImage = (req, res) => {
  res.setHeader("Content-Type", req.query.mimetype);
  const pathImg = path.join(config.uploadPath, req.params.hash);
  const exist = fs.existsSync(pathImg);
  if (exist) {
    fs.createReadStream(pathImg).pipe(res);
  } else {
    res.end();
  }
};

exports.index = async (req, res) => {
  const data = await Model.find({ ativo: true })
    .skip(req.body.skip || 0)
    .limit(req.body.limit || 50);

  const total = await Model.find().count();

  res.json({ total, data });
};

exports.show = async (req, res) => {
  const data = await Model.findOne(
    { _id: req.params.id, ativo: true },
    { password: 0 }
  );
  res.json(data);
};

exports.new = async (req, res) => {
  var model = new Model(req.body);
  const data = await model.save();

  if (!err && data) {
    res.json({ success: true, data, form: req.body });
  } else {
    res.json({ succsess: false, data, form: req.body });
  }
};

exports.delete = async (req, res) => {
  const model = await Model.findOne({ _id: req.params.id });

  model.ativo = false;

  const data = await model.save();

  if (data) {
    res.json({ success: true, data, form: req.body });
  } else {
    res.json({ success: false, data, form: req.body });
  }
  res.json(data);
};

exports.edit = async (req, res) => {
  const model = await Model.findOne({ _id: req.params.id, ativo: true });

  if (model) {
    if (req.body.password && req.body.password !== "") {
      model.password = req.body.password;
    }

    model.login = req.body.login;
    model.nome = req.body.nome;
    model.type = req.body.type;
    model.name = req.body.name;
    model.role = req.body.role;
    model.cep = req.body.cep;
    model.estado = req.body.estado;
    model.cidade = req.body.cidade;
    model.rua = req.body.rua;
    model.bairro = req.body.bairro;
    model.numero = req.body.numero;
    model.mimetype = req.body.mimetype;
    model.filename = req.body.filename;
    model.originalname = req.body.originalname;
    model.nascimento = req.body.nascimento;

    if (model.type === "cpf") {
      model.escola = req.body.escola;
      model.matricula = req.body.matricula;
      model.nomeMae = req.body.nomeMae;
      model.hobees = req.body.hobees;
    }

    if (model.type === "cnpj") {
      model.ramoEmpresa = req.body.ramoEmpresa;
      model.atividadeEmpresa = req.body.atividadeEmpresa;
    }

    const data = await model.save();

    if (data) {
      res.json({ success: true, data, form: req.body });
    } else {
      res.json({ success: false, data, form: req.body });
    }
  } else {
    res.json({
      success: false,
      data: { text: "usuario não encontrado" },
      form: req.body,
    });
  }
};
