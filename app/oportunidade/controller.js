const Model = require("./model");

exports.index = async (req, res) => {
  const filtro = {};
  filtro.ativo = true;

  if (req.query.usuario) {
    filtro.usuario = req.query.usuario;
  }

  const data = await Model.find(filtro)
    .skip(req.query.skip || 0)
    .limit(req.query.limit || 50);

  const total = await Model.find(filtro).count();

  res.json({ total, data });
};

exports.show = async (req, res) => {
  const data = await Model.findOne(
    { _id: req.params.id, ativo: true },
    { password: 0 }
  );
  res.json(data);
};

exports.new = async (req, res) => {
  var model = new Model(req.body);
  const data = await model.save();

  if (data) {
    res.json({ success: true, data, form: req.body });
  } else {
    res.json({ succsess: false, data, form: req.body });
  }
};

exports.delete = async (req, res) => {
  const model = await Model.findOne({ _id: req.params.id });

  model.ativo = false;

  const data = await model.save();

  if (data) {
    res.json({ success: true, data, form: req.body });
  } else {
    res.json({ success: false, data, form: req.body });
  }
  res.json(data);
};

exports.edit = async (req, res) => {
  const model = await Model.findOne({ _id: req.params.id, ativo: true });

  if (model) {
    if (req.body.password && req.body.password !== "") {
      model.password = req.body.password;
    }

    model.area = req.body.area;
    model.cargaHoraria = req.body.cargaHoraria;
    model.empresa = req.body.empresa;
    model.endereco = req.body.endereco;
    model.sobre = req.body.sobre;
    model.valor = req.body.valor;
    model.usuario = req.body.usuario;
    model.candidatos = req.body.candidatos;

    const data = await model.save();

    if (data) {
      res.json({ success: true, data, form: req.body });
    } else {
      res.json({ success: false, data, form: req.body });
    }
  } else {
    res.json({
      success: false,
      data: { text: "oportunidade não encontrado" },
      form: req.body,
    });
  }
};
