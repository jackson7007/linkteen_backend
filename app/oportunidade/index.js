const express = require("express");
const rotas = express.Router();

const controller = require("./controller");

rotas.post("/", controller.new);
rotas.put("/:id", controller.edit);
rotas.get("/", controller.index);
rotas.get("/:id", controller.show);
rotas.delete("/:id", controller.delete);

module.exports = rotas;
