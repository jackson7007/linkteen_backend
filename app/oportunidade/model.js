const mongoose = require("mongoose");

const Schema = mongoose.Schema({
  area: String,
  cargaHoraria: String,
  empresa: String,
  endereco: String,
  sobre: String,
  ativo: { type: Boolean, default: true },
  valor: Number,
  ferramentaRotina: String,
  usuario: {
    type: mongoose.Schema.ObjectId,
    ref: "Usuario",
  },
  candidatos: [
    {
      usuario: {
        type: mongoose.Schema.ObjectId,
        ref: "Usuario",
      },
      acept: { type: Boolean, default: false },
    },
  ],
});

module.exports = mongoose.model("Oportunidade", Schema);
