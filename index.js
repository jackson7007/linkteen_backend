var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var server = require("http").Server(app);
var cors = require("cors");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());

app.get("/api/", function (req, res, next) {
  res.json("online");
});

var config = require("./config");
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE,
  useMongoClient: true,
});

/*Login de Usuarios*/
app.use("/api/", require("./app/usuario/auth"));
app.use("/users", require("./app/usuario"));
app.use("/api/opportunity", require("./app/oportunidade"));

/*Mid para rotas da API verificar JWT*/
var jwt = require("./core/jwt");
app.use("/api/v1", jwt);

/*Modulos*/
/* jwt.use("/users", require("./app/usuario"));
jwt.use("/opportunity", require("./app/oportunidade")); */

var port = parseInt(config.initialPort);

server.listen(port, "127.0.0.1");
console.log("Server start: " + port);
